<?php
$nombre = $_POST['nombre']
$apellido = $_POST['apellido']
$email = $_POST['email']
$phone = $_POST['phone']
$giro = $_POST['giro']
$localidad = $_POST['localidad']
$comentario = $_POST['comentario']

$header = 'From: ' . $email . "\r\n";
$header .= "X-Mailer: PHP/" . phpversion() . "\r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$message = "Este mensaje fue enviado por: " . $nombre . "\r\n";
$message = "Su email es: " . $email . "\r\n";
$message = "Teléfono de contacto: " . $phone . "\r\n";
$message = "Giro industrial: " . $giro . "\r\n";
$message = "Localidad: " . $localidad . "\r\n";
$message = "Mensaje: " . $_POST['comentario'] . "\r\n";
$message = "Enviado el: " . date('d/m/Y', time());

$para = 'ventas@rodadeferro.com';
$asunto = 'Mensaje de cliente web';

mail($para, $asunto, utf8_decode($message), $header);

header("Location:index.html");
?>
